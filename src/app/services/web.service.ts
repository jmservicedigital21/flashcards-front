import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Card} from '../interfaces/card';
import {Reponse} from '../interfaces/reponse';

@Injectable({
  providedIn: 'root'
})
export class WebService {
 private  BASE_URL = 'http://localhost:3000';
  cards = [];
  error = null;
reponses = []
  constructor(private http: HttpClient) {
    this.getCards();
  }
  getCards(){
   this.http.get<Card[]>(`${this.BASE_URL}/cards`).subscribe(data => {
      this.cards = data;
      /*console.log('cards', data);*/
      this.reponses = [];
    }, error => {
      this.error = error;
      console.error(error);
    });
}
  saveReponse(reponse) {
  this.reponses = [...this.reponses, reponse];
  console.log('this.reponses', this.reponses);
  }
  get allCardsReponse() {
  return this.reponses.length === this.cards.length;
  }
  saveAllReponsesToServer(){
  return this.http.post<Reponse[]>(`${this.BASE_URL}/cards`, this.reponses);
  }
  reset(){
  return this.http.post<{ reset: true}>(`${this.BASE_URL}/cards/reset`,{ reset: true });
  }
}
