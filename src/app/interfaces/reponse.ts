import {Card} from './card';

export interface Reponse {
  card: Card;
  isVrai: boolean;
}
