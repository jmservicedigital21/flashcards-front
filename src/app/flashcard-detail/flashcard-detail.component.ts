import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-flashcard-detail',
  templateUrl: './flashcard-detail.component.html',
  styleUrls: ['./flashcard-detail.component.scss']
})
export class FlashcardDetailComponent implements OnInit {
@Input() card;
@Output() reponse = new EventEmitter()
  showReponse = false;
  constructor() { }

  ngOnInit(): void {
  }
flip() {
  this.showReponse = !this.showReponse;

}
  setReponse(isVrai){
    console.log('isVrai',isVrai);
    this.reponse.emit({card: this.card, isVrai: isVrai });

  }
}
