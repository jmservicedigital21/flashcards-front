import { Component, OnInit } from '@angular/core';
import { WebService } from '../services/web.service';

@Component({
  selector: 'app-flashcard-list',
  templateUrl: './flashcard-list.component.html',
  styleUrls: ['./flashcard-list.component.scss']
})
export class FlashcardListComponent implements OnInit {

  constructor(public web: WebService ) { }

  ngOnInit(): void {
  }
saveReponse(reponse) {
    console.log('reponse', reponse);
    this.web.saveReponse(reponse);
}
saveAll(){
    this.web.saveAllReponsesToServer().subscribe(data => {
      console.log('saveAllReponsesToServer SUCCESS', data);
      this.web.getCards();
    }, err => {
      console.error('saveAllReponesToServer FAILLURE', err);
    });
}
reset() {
    this.web.reset().subscribe(data => {
      console.log('reset SUCCESS', data);
      this.web.getCards();
    }, err =>{
      console.error('reset FAILURE')
    });
}
}
